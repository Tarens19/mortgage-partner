import * as functions from "firebase-functions";
import * as admin from 'firebase-admin';
import 'firebase-functions';
admin.initializeApp(functions.config().firebase);

// Sendgrid Config

const API_KEY = functions.config().sendgrid.key;
// const TEMPLATE_ID = functions.config().sendgrid.template;
const APPLICATION_CREATED = functions.config().sendgrid.application_created;
const DEFAULT_ACTION = functions.config().sendgrid.default_action;
const DEFAULT_NOACTION = functions.config().sendgrid.default_noaction;
// const cors = require('cors')({ origin: true });

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(API_KEY);



exports.getSubCollections = functions.https.onCall(async (data) => {

    const docPath = data.docPath;

    const collections = await admin.firestore().doc(docPath).listCollections();
    const collectionIds = collections.map((col: any) => col.id);

    return { collections: collectionIds };

});

exports.welcomeEmail = functions.firestore
    .document('/users/{userId}/userInfo/data')
    .onCreate((snap, event) => {
        const user = snap.data();
        var userName = user.displayName
        if(!user.displayName) userName = user.firstName + ' ' + user.lastName;
        const msg = {
            to: user.emailAddress,
            from: 'tyler@mortgagepartner.com',
            templateId: DEFAULT_ACTION,
            dynamic_template_data: {
                subject: '🤝 Hi ' + user.firstName + '! Welcome To Mortgage Partner - We Are Your Partner For Life',
                name: userName,
                title: 'Welcome, ' + user.firstName + '!',
                mainText: 'Hi ' + user.firstName + ', welcome to Mortgage Partner.',
                subText: 'You now have a partner in the mortgage industry. We offer Conventional, FHA, VA, and USDA loans at excellent rates. Apply online via our website or give us a call at <a href="tel:8013020100">801-302-0100</a>.',
                buttonLink: 'https://mortgagepartner.com/apply',
                buttonText: 'Apply Online'
            },
        };
        return sgMail.send(msg);
    });

exports.adminWelcomeEmail = functions.firestore
    .document('/users/{userId}/userInfo/data')
    .onCreate((snap, event) => {
        const userId = event.params.userId;
        const user = snap.data();
        var userName = user.displayName
        if(!user.displayName) userName = user.firstName + ' ' + user.lastName;
        const msg = {
            to: ['tyler@mortgagepartner.com', 'processor@mortgagepartner.com', 'penny@mortgagepartner.com'],
            from: 'tyler@mortgagepartner.com',
            templateId: DEFAULT_ACTION,
            dynamic_template_data: {
                subject: '🤝 ' + userName + ' Created a Mortgage Partner Account',
                name: userName,
                title: 'New Account Created',
                mainText: userName + ' created a new account',
                subText: "email: " + user.emailAddress + " <br> " + "phone number: " + ' <a href="tel:' + user.phoneNumber + '">' + user.phoneNumber + "</a>",
                buttonLink: 'https://console.firebase.google.com/u/0/project/mortgage-partner/firestore/data/~2Fusers~2F' + userId + '~2FuserInfo~2Fdata',
                buttonText: 'Go to user profile'
            },
        };
        return sgMail.send(msg);
    });

exports.newApplicationEmail = functions.firestore
    .document('/users/{userId}/applications/{documentId}')
    .onCreate((snap, event) => {

        // Grab the current value of what was written to Firestore.
        // const original = snap.data().original;

        const userId = event.params.userId;

        const db = admin.firestore();

        return db.collection('users').doc(userId).collection('userInfo').doc('data')
            .get()
            .then((doc: any) => {

                const user = doc.data()

                const msg = {
                    to: user.emailAddress,
                    from: 'tyler@mortgagepartner.com',
                    // text: `Hey ${toName}. You have a new follower!!! `,
                    // html: `<strong>Hey ${toName}. You have a new follower!!!</strong>`,

                    // custom templates
                    templateId: APPLICATION_CREATED,
                    dynamic_template_data: {
                        subject: '⭐ Mortgage Partner Application Received',
                        name: user.firstName,
                        title: 'Application Received',
                        mainText: 'Hi ' + user.firstName + ', thank you for trusting us with your mortgage! Your application has been received and our loan experts are reviewing the application.',
                        subText: 'To speed up your mortgage process, please review and upload items we need to get you started.',
                        buttonLink: 'https://mortgagepartner.com/account/items-needed',
                        buttonText: 'View Items Needed'
                    },
                };

                return sgMail.send(msg)
            })
            .then(() => console.log('email sent!'))
            .catch(err => console.log(err))


    });

exports.adminApplicationNotificationEmail = functions.firestore
    .document('/users/{userId}/applications/{documentId}')
    .onCreate((snap, event) => {

        // Grab the current value of what was written to Firestore.
        // const original = snap.data().original;

        const userId = event.params.userId;

        const db = admin.firestore();

        return db.collection('users').doc(userId).collection('userInfo').doc('data')
            .get()
            .then((doc: any) => {

                const user = doc.data()

                const msg = {
                    to: ['tyler@mortgagepartner.com', 'processor@mortgagepartner.com', 'penny@mortgagepartner.com'],
                    from: 'tyler@mortgagepartner.com',
                    // text: `Hey ${toName}. You have a new follower!!! `,
                    // html: `<strong>Hey ${toName}. You have a new follower!!!</strong>`,

                    // custom templates
                    templateId: APPLICATION_CREATED,
                    dynamic_template_data: {
                        subject: '⭐ Mortgage Partner Application For ' + user.firstName + ' ' + user.lastName + ' ⭐',
                        name: user.firstName,
                        title: 'New Application',
                        mainText: user.firstName + ' ' + user.lastName + ' submitted a new online application. <br> email: ' + user.emailAddress + " <br> " + "phone number: " + ' <a href="tel:' + user.phoneNumber + '">' + user.phoneNumber + "</a>",
                        subText: 'Click the button below to view the application.',
                        buttonLink: 'https://console.firebase.google.com/u/0/project/mortgage-partner/firestore/data/~2Fusers~2F' + userId + '~2Fapplications~2F' + doc.id,
                        buttonText: 'View Application'
                    },
                };

                return sgMail.send(msg)
            })
            .then(() => console.log('email sent!'))
            .catch(err => console.log(err))


    });

exports.adminUploadNotificationEmail = functions.firestore
    .document('/uploads/{documentId}')
    .onCreate((snap, event) => {

        // Grab the current value of what was written to Firestore.
        // const original = snap.data().original;

        const doc = snap.data()

        const msg = {
            to: ['tyler@mortgagepartner.com', 'processor@mortgagepartner.com', 'penny@mortgagepartner.com'],
            from: 'Tyler@mortgagepartner.com',
            // text: `Hey ${toName}. You have a new follower!!! `,
            // html: `<strong>Hey ${toName}. You have a new follower!!!</strong>`,

            // custom templates
            templateId: DEFAULT_ACTION,
            dynamic_template_data: {
                subject: '📄 ' + doc.user + ' Uploaded to ' + doc.fileForItem + ' 📄',
                name: doc.user,
                title: doc.user + ' Uploaded A New Document',
                mainText: 'Document: <strong>' + doc.originalName + ' </strong> <br> Uploaded to: <strong>' + doc.fileForItem + '</strong>',
                // subText: 'Click the button below to view the application.',
                buttonLink: doc.downloadURL,
                buttonText: 'Download Upload'
            },
        };

        return sgMail.send(msg)

    });




    exports.giveawayEmail = functions.firestore
    .document('/giveaways/Golf-to-Give/{documentId}/data')
    .onCreate((snap, event) => {
        const user = snap.data();
        var userName = user.firstName + ' ' + user.lastName;
        const msg = {
            to: user.emailAddress,
            from: 'tyler@mortgagepartner.com',
            templateId: DEFAULT_ACTION,
            dynamic_template_data: {
                subject: "⛳️  " + user.firstName + ", You’ve Entered Mortgage Partner’s Giveaway",
                name: userName,
                title: "Successfully Entered Giveaway!",
                mainText: 'Hi ' + user.firstName + ', you entered our giveaway for ' + user.event + '!',
                subText: "You now have a partner in the mortgage industry. We are a family-owned mortgage brokerage that offers Conventional, FHA, VA, and USDA loans at excellent rates. We will always put your client’s needs first. <br/> <br/> Let’s chat about what we can offer for you and your clients, <a href='tel:8013020100'>801-302-0100</a>.",
                buttonLink: 'https://mortgagepartner.com/',
                buttonText: 'Learn More'
            },
        };
        return sgMail.send(msg);
    });

    exports.giveawayEmailAdmin = functions.firestore
    .document('/giveaways/Golf-to-Give/{documentId}/data')
    .onCreate((snap, event) => {
        const user = snap.data();
        var userName = user.firstName + ' ' + user.lastName;
        const msg = {
            to: ['tyler@mortgagepartner.com', 'processor@mortgagepartner.com', 'penny@mortgagepartner.com'],
            from: 'tyler@mortgagepartner.com',
            templateId: DEFAULT_ACTION,
            dynamic_template_data: {
                subject: "⛳️  " + userName + " Entered Mortgage Partner’s Golf to Give Giveaway",
                name: userName,
                title: user.event + " Giveaway!",
                mainText: userName + ' entered our giveaway for ' + user.event + '!',
                subText: "Name: " + userName + " <br> " + "Email: " + user.emailAddress + " <br> " + "Phone Number: " + ' <a href="tel:' + user.phoneNumber + '">' + user.phoneNumber + "</a>" + " <br> " + "Occupation: " + user.occupation + " <br> " + "Date Entered: " + user.submissionDate + " <br> ",
                buttonLink: 'https://console.firebase.google.com/u/0/project/mortgage-partner/firestore/data/~2Fgiveaways~2FGolf-to-Give~2F' + userName + '~2Fdata',
                buttonText: 'Go to user data'
            },
        };
        return sgMail.send(msg);
    });



// Sends email via HTTP. Can be called from frontend code. 
export const genericEmail = functions.https.onCall(async (data, context) => {

    if (!context.auth) {
        throw new functions.https.HttpsError('failed-precondition', 'Must be logged with an email address');
    }

    const msg = {
        to: context.auth.token.email,
        from: 'tyler@mortgagepartner.com',
        templateId: DEFAULT_NOACTION,
        dynamic_template_data: {
            subject: data.subject,
            name: data.name,
            title: data.title,
            mainText: data.mainText,
            subText: data.subText,
            buttonLink: data.buttonLink,
            buttonText: data.buttonText,
        },
    };

    await sgMail.send(msg);

    // Handle errors here

    // Response must be JSON serializable
    return { success: true };

});
// Sends email via HTTP. Can be called from frontend code. 
export const genericActionEmail = functions.https.onCall(async (data, context) => {

    if (!context.auth) {
        throw new functions.https.HttpsError('failed-precondition', 'Must be logged with an email address');
    }

    const msg = {
        to: context.auth.token.email,
        from: 'tyler@mortgagepartner.com',
        templateId: DEFAULT_ACTION,
        dynamic_template_data: {
            subject: data.subject,
            name: data.name,
            title: data.title,
            mainText: data.mainText,
            subText: data.subText,
            buttonLink: data.buttonLink,
            buttonText: data.buttonText,
        },
    };

    await sgMail.send(msg);

    // Handle errors here

    // Response must be JSON serializable
    return { success: true };

});

    // Emails the author when a new comment is added to a post
// export const newComment = functions.firestore.document('articles/{articleId}/comments/{commentId}').onCreate( async (change, context) => {

//     // Read the post document
//     const postSnap = await db.collection('posts').doc(context.params.postId).get();

//     // Raw Data
//     const post = postSnap.data();
//     const comment = change.data();

//     // Email
//     const msg = {
//         to: post.authorEmail,
//         from: 'hello@fireship.io',
//         templateId: TEMPLATE_ID,
//         dynamic_template_data: {
//             subject: `New Comment on ${post.title}`,
//             name: post.displayName,
//             text: `${comment.user} said... ${comment.text}`
//         },
//     };

//     // Send it
//     return sgMail.send(msg);

// });