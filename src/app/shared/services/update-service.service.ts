import { isPlatformBrowser } from '@angular/common';
import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { interval } from 'rxjs';
import { WindowRef } from './window.service';

@Injectable()
export class UpdateService {

	constructor(
		public updates: SwUpdate,
		@Inject(PLATFORM_ID) private platformId: any,
		private windowRef: WindowRef
	) {
		if (updates.isEnabled) {
			interval(6 * 60 * 60).subscribe(() => updates.checkForUpdate()
				.then(() => console.log('checking for updates')));
		}
	}

	public checkForUpdates(): void {
		console.log('Checking if an update is available.');
		this.updates.available.subscribe(event => this.promptUser());
	}

	// private promptUser(): void {
	//   console.log('updating to new version');
	//   this.updates.activateUpdate().then(() => document.location.reload())); 
	// }
	private promptUser(): void {
		console.log('New version available.');
		this.updates.available.subscribe(() => {

			if (confirm("New version available. Load new version of The Mortgage Partner?")) {

				if (isPlatformBrowser(this.platformId)) {
					// Use the window reference: this.windowRef
					this.windowRef.nativeWindow.location.reload()
				}
				// else window.location.reload();

			}
		});
	}
}