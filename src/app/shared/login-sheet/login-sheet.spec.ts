import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginBottomSheet } from './login-sheet';


describe('LoginBottomSheet', () => {
  let component: LoginBottomSheet;
  let fixture: ComponentFixture<LoginBottomSheet>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginBottomSheet ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginBottomSheet);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
