import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnersNikoComponent } from './niko.component';

describe('PartnersNikoComponent', () => {
  let component: PartnersNikoComponent;
  let fixture: ComponentFixture<PartnersNikoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnersNikoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnersNikoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
