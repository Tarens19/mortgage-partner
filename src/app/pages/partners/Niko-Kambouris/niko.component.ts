import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { fxArray } from 'src/app/models';
import { AuthService } from 'src/app/shared';

@Component({
  selector: 'partner-niko',
  templateUrl: './niko.component.html',
  styleUrls: ['./niko.component.scss'],
  animations: fxArray
})
export class PartnersNikoComponent implements OnInit, AfterViewInit {
  @ViewChild("topOfPage", { static: false }) scrollToTopOfPage: ElementRef;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public auth: AuthService,
  ) { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.scrollTo('topOfPage');
    }, 100);
  }

  async scrollTo(destination: string) {
    if (destination === 'topOfPage') this.scrollToTopOfPage.nativeElement.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });
  }

}
