import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PartnersAshleeStrongComponent, PartnersNikoComponent } from '.';
import { HomepageComponent } from '..';


const routes: Routes = [
	{
		path: '',
		children: [
			{ path: '', component: HomepageComponent, data: { title: 'Dashboard' } },
			// { path: 'account-settings', component: AccountSettingsComponent, data: { title: 'Account settings' } },
			{ path: 'niko', component: PartnersNikoComponent, data: { title: 'Niko Kambouris' } },
			{ path: 'niko-kambouris', component: PartnersNikoComponent, data: { title: 'Niko Kambouris' } },
			{ path: 'nicholas-kambouris', component: PartnersNikoComponent, data: { title: 'Niko Kambouris' } },
			{ path: 'ashlee', component: PartnersAshleeStrongComponent, data: { title: 'Ashlee Strong' } },
			{ path: 'ashlee-strong', component: PartnersAshleeStrongComponent, data: { title: 'Ashlee Strong' } },
		]
	},
	{ path: '**', redirectTo: '/404', pathMatch: 'full' }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class PartnersRoutingModule { }
