import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { interval } from 'rxjs';
import { fxArray } from 'src/app/models';
import { AuthService } from 'src/app/shared';

@Component({
  selector: 'partner-ashlee-strong',
  templateUrl: './ashlee-strong.component.html',
  styleUrls: ['./ashlee-strong.component.scss'],
  animations: fxArray
})
export class PartnersAshleeStrongComponent implements OnInit, AfterViewInit {
  @ViewChild("topOfPage", { static: false }) scrollToTopOfPage: ElementRef;

  testimonial1: boolean = true
  testimonial2: boolean = false
  testimonial3: boolean = false
  testimonials = [
    { quote: "Ashlee worked so hard to make sure we got into a home that we loved! She was so easy to work with, so kind in explaining every detail/answering all our questions, and found us the perfect home- even in the craziness of the market. She was incredible, we loved working with her so much we recommended her to my sister and they loved working with her as well. Nothing but good things to say about Ashlee’s work. She’s an expertise in all things realty and we’re so grateful for all that she’s done for our family! Truly a 10/10 experience!", name: "Casey H." },
    { quote: "We loved working with Ashlee as we sold and bought our home. She was very thorough and answered all our questions. She was willing and happy to meet us at any home we wanted to see. She put our desires and satisfaction first. She is professional and knowledgeable with a hard work ethic. She’s kind and easy going, we’re happy to call her a new friend! She was determined and resilient in helping us find our dream home amidst the chaos of the market. We are grateful for her hard work and expertise that led us to our new home that we love.", name: "Jordan H." },
    { quote: "I’ve used Ashlee on multiple occasions to buy & sell properties. She has an extensive working knowledge of the industry, new and existing constructions, and the overall buying and selling processes. She will always put your needs at the forefront to ensure a smooth and beneficial transaction. If you’re looking for a top-tier agent who will work tirelessly to ensure your needs are met, call Ashlee!", name: "Andrew L." },
  ]
  showTestimonialIndex: number = 0;


  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public auth: AuthService,
  ) { }

  ngOnInit(): void { }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.scrollTo('topOfPage');
    }, 100);
    // Cycle through testimonials.
    interval(15000).subscribe(x => {
      // Check if we have more testimonials to cycle through. Else go back to 0,
      if (this.showTestimonialIndex < (this.testimonials.length - 1)) this.nextTestimonial();
      else this.showTestimonialIndex = 0;
    });
  }

  async scrollTo(destination: string) {
    if (destination === 'topOfPage') this.scrollToTopOfPage.nativeElement.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });
  }

  nextTestimonial() {
    if (this.showTestimonialIndex < (this.testimonials.length - 1)) this.showTestimonialIndex = this.showTestimonialIndex + 1;
    else this.showTestimonialIndex = 0;
  }

  prevTestimonial() {
    if (this.showTestimonialIndex === 0) this.showTestimonialIndex = (this.testimonials.length - 1);
    else this.showTestimonialIndex = this.showTestimonialIndex - 1;
  }

}
