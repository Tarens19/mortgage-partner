import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnersAshleeStrongComponent } from './ashlee-strong.component';

describe('PartnersAshleeStrongComponent', () => {
  let component: PartnersAshleeStrongComponent;
  let fixture: ComponentFixture<PartnersAshleeStrongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnersAshleeStrongComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnersAshleeStrongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
