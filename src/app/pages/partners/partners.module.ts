import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';
import { FileSelectDirective } from 'ng2-file-upload';
import { AppCommonModule } from 'src/app/app-common.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { PartnersAshleeStrongComponent, PartnersNikoComponent } from '.';
import { PartnersRoutingModule } from './partners.routing';

@NgModule({
	imports: [
		AppCommonModule,
		SharedModule,
		TextMaskModule,
		PartnersRoutingModule,
	],
	declarations: [
		PartnersNikoComponent,
		PartnersAshleeStrongComponent
	],
	providers: [
		// Services here
		FileSelectDirective
	],
	exports: [],
	entryComponents: [
		// Dialogs, bottomsheets. etc. here.
	]
})
export class PartnersModule {
	constructor() { console.log('@ModuleCreated::PartnersModule'); }
}
