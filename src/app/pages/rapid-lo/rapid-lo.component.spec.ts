import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RapidLoComponent } from './rapid-lo.component';

describe('RapidLoComponent', () => {
  let component: RapidLoComponent;
  let fixture: ComponentFixture<RapidLoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RapidLoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RapidLoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
