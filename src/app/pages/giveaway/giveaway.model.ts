export class Giveaway {
    firstName: string;
    lastName: string;
    user: string;
    userIp: string;
    phoneNumber: number;
    emailAddress: string;
    occupation: string;
    hearAbout: string;
    acceptedTerms: boolean;
    event: string;
    submissionDate: any;
    docId: string;
}