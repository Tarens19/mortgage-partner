import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';
import { FileSelectDirective } from 'ng2-file-upload';
import { AppCommonModule } from 'src/app/app-common.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { GolfToGiveComponent } from '.';
import { GiveawayRoutingModule } from './giveaway.routing';

@NgModule({
	imports: [
		AppCommonModule,
		SharedModule,
		TextMaskModule,
		GiveawayRoutingModule,
	],
	declarations: [
		GolfToGiveComponent
	],
	providers: [
		// Services here
		FileSelectDirective
	],
	exports: [],
	entryComponents: [
		// Dialogs, bottomsheets. etc. here.
	]
})
export class GiveawayModule {
	constructor() { console.log('@ModuleCreated::GiveawayModule'); }
}
