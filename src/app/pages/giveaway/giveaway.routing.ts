import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GolfToGiveComponent } from '.';
import { HomepageComponent } from '..';


const routes: Routes = [
	{
		path: '',
		children: [
			{ path: '', component: HomepageComponent, data: { title: '' } },
			// { path: 'account-settings', component: AccountSettingsComponent, data: { title: 'Account settings' } },
			{ path: 'golf-to-give', component: GolfToGiveComponent, data: { title: 'Golf To Give' } },
		]
	},
	{ path: '**', redirectTo: '/404', pathMatch: 'full' }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class GiveawayRoutingModule { }
