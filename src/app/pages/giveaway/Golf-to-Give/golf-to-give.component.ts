import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, NgZone, ElementRef, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { fxArray } from 'src/app/models';
import { FirebaseErrors } from 'src/app/shared/models/error.model';
import { GiveawayService } from 'src/app/shared/services/giveaway.service';
import { textMaskPhoneFn } from 'src/app/shared/services/text-mask-functions';
import { Giveaway } from '../giveaway.model';

@Component({
  templateUrl: './golf-to-give.component.html',
  styleUrls: ['./golf-to-give.component.scss'],
  animations: fxArray
})
export class GolfToGiveComponent implements OnInit, AfterViewInit {
  @ViewChild("topOfPage", { static: false }) scrollToTopOfPage: ElementRef;
  @ViewChild("completedForm", { static: false }) scrollToCompletedForm: ElementRef;
  @ViewChild('caForm', { static: false }) private caForm: NgForm;
  public giveaway: Giveaway = new Giveaway();
  public mask = { guide: false, mask: textMaskPhoneFn };
  isLoading: boolean = false;
  formCompleted: boolean = false;
  ipAddress = '';
  giveawayEvent = 'Golf to Give';
  docId= 'Golf-to-Give';
  animate: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public giveawayService: GiveawayService,
    public ngZone: NgZone,
    public snackBar: MatSnackBar,
    private http:HttpClient
  ) { }

  ngOnInit(): void {
    this.getIPAddress();
   }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.scrollTo('topOfPage');
    }, 100);
  }
  
  getIPAddress()
  {
    this.http.get("https://api.ipify.org/?format=json").subscribe((res:any)=>{
      this.ipAddress = res.ip;
    });
  }

  async scrollTo(destination: string) {
    if (destination === 'topOfPage') this.scrollToTopOfPage.nativeElement.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });
    if (destination === 'completedForm') this.scrollToTopOfPage.nativeElement.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });
  }



  async createGiveawayUser() {
    this.isLoading = true;
    if (this.caForm.valid) {
      this.formCompleted = true;
      this.giveaway.user = this.giveaway.firstName + ' ' + this.giveaway.lastName
      this.giveaway.userIp = this.ipAddress;
      this.giveaway.event = this.giveawayEvent;
      this.giveaway.docId = this.docId;
      this.giveaway.submissionDate = moment().format('MM/DD/YYYY LT')
      this.giveawayService.createGiveawayData(this.giveaway)

        .then((user) => {
          this.ngZone.run(() => {
            this.isLoading = false;
            setTimeout(() => {
              this.animate = true;
            }, 100);
          });

        }).catch((error) => {
          this.isLoading = false;
          if (error) {
            console.log("🚀error: ", error)
            this.snackBar.open(FirebaseErrors.parse(error.code), "Close", { duration: 3000, panelClass: ["error"] });
          }
        });
    } else {
      this.isLoading = false;
      this.snackBar.open("Invalid form data", "Close", { duration: 3000, panelClass: ["error"] });
    }
  }

}
