import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GolfToGiveComponent } from './golf-to-give.component';

describe('GolfToGiveComponent', () => {
  let component: GolfToGiveComponent;
  let fixture: ComponentFixture<GolfToGiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GolfToGiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GolfToGiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
