import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';
import { FileSelectDirective } from 'ng2-file-upload';
import { AppCommonModule } from 'src/app/app-common.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AccountComponent, AccountSettingsComponent, ApplicationsComponent, ItemsNeededComponent, ItemsUploadedComponent } from '.';
import { AccountRoutingModule } from './account.routing';

@NgModule({
	imports: [
		AppCommonModule,
		SharedModule,
		TextMaskModule,
		AccountRoutingModule,
	],
	declarations: [
		AccountComponent,
		AccountSettingsComponent,
		ItemsNeededComponent,
		ItemsUploadedComponent,
		ApplicationsComponent
	],
	providers: [
		// Services here
		FileSelectDirective
	],
	exports: [],
	entryComponents: [
		// Dialogs, bottomsheets. etc. here.
	]
})
export class AccountModule {
	constructor() { console.log('@ModuleCreated::ToolsModule'); }
}
