import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccountComponent, AccountSettingsComponent, ApplicationsComponent, ItemsNeededComponent, ItemsUploadedComponent } from '.';


const routes: Routes = [
	{
		path: '',
		children: [
			{ path: '', component: AccountComponent, data: { title: 'Dashboard' } },
			// { path: 'account-settings', component: AccountSettingsComponent, data: { title: 'Account settings' } },
			{ path: 'applications', component: ApplicationsComponent, data: { title: 'Applications' } },
			{ path: 'items-needed', component: ItemsNeededComponent, data: { title: 'Items Needed' } },
			{ path: 'items-uploaded', component: ItemsUploadedComponent, data: { title: 'Items Uploaded' } },
		]
	},
	{ path: '**', redirectTo: '/404', pathMatch: 'full' }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class AccountRoutingModule { }
