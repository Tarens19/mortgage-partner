import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { interval } from 'rxjs';
import { AppState, fxArray } from 'src/app/models';
import { AuthService } from 'src/app/shared';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss'],
  animations: fxArray
})
export class ApplicationsComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("topOfPage", { static: false }) scrollToTopOfPage: ElementRef;
  appsColumns: string[] = ['icon', 'type', 'status', 'propAddress', 'date', 'loanAmount', 'id', 'action'];

  waitingForUser: boolean = true;
  animate: boolean = false;
  index: number = 0;
  interval: any;
  filter: string = '';

  constructor(
    public appState: AppState,
    public snackBar: MatSnackBar,
    public auth: AuthService,
    public store: UserService,
  ) {
  }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.interval = interval(250).subscribe(x => {
      this.checkForUser();
    });
    setTimeout(() => {
      this.scrollTo('topOfPage');
    }, 100);
  }

  ngOnDestroy(): void { }

  checkForUser() {
    this.index++
    if (!this.store.isLoadingUser) {
      if ((this.store.userInfo$?.value.uid != undefined || null) && this.auth.isAuthenticated) {
        this.store.getApplications();
        setTimeout(() => {
          this.waitingForUser = false;
        }, 100);
        this.interval.unsubscribe();
      } else {
        console.log("Not logged in");
        if (this.index === 50) {
          setTimeout(() => {
            this.waitingForUser = false;
          }, 100);
          this.store.showApps = false;
          this.interval.unsubscribe();
        }
      }
    }
  }

  async scrollTo(destination: string) {
    if (destination === 'topOfPage') this.scrollToTopOfPage.nativeElement.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });
  }

  // Filter
  async applyFilter(filterValue: string) {
    this.store.applicationSource.filter = filterValue.trim().toLowerCase();
  }

  async resetFilter() {
    this.filter = '';
    this.applyFilter('');
  }

}

