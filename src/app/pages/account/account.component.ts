import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Location } from '@angular/common';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { fxArray } from 'src/app/models';
import { AuthService, LoginBottomSheet } from 'src/app/shared';
import { ApplicationData, UserInfo } from 'src/app/shared/models/user.model';
import { textMaskPhoneFn } from 'src/app/shared/services/text-mask-functions';
import { UserService } from 'src/app/shared/services/user.service';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss'],
  animations: fxArray
})
export class AccountComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("topOfPage", { static: false }) scrollToTopOfPage: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.store.itemsNeededSource.paginator = this.paginator;
    this.store.itemsNeededSource.sort = this.sort;
  }

  needItems: boolean = false;
  userData: UserInfo;
  timer: any;
  waitingForUser: boolean = true;
  editProfile: boolean = false;
  index: number = 0;
  interval: any;
  public mask = { guide: false, mask: textMaskPhoneFn };
  appsColumns: string[] = ['icon', 'type', 'status', 'propAddress', 'date', 'loanAmount', 'id', 'action'];
  itemsColumns: string[] = ['icon', 'item', 'borrower', 'priorTo'];
  uploadsColumns: string[] = ['icon', 'name', 'date', 'for', 'action'];

  constructor(
    public afAuth: AngularFireAuth,
    public store: UserService,
    public auth: AuthService,
    private _bottomSheet: MatBottomSheet,
    public snackBar: MatSnackBar,
    private _location: Location,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.waitingForUser = true;
    this.store.accAppData$.next(undefined);
    this.afAuth.authState.subscribe((user: any) => {
      if (user) {
        this.store.getUser(user.uid);

        this.userData = this.store.userInfo$.value;
        this.waitingForUser = false;
        this.store.getUserData();
      } else {
        localStorage.setItem('user', null);
        this.store.clearUserData();
        this.waitingForUser = false;
        this.showLogin();
      }
    });
  }

  ngAfterViewInit() {

    // this.interval = interval(250).subscribe(x => {
    //   this.checkForUser();
    // });
    setTimeout(() => {
      this.scrollTo('topOfPage');
    }, 100);
    setTimeout(() => {
      this.setDataSourceAttributes();
    }, 1500);
  }


  ngOnDestroy(): void { }

  updateProfile() {
    this.waitingForUser = true;
    if (this.store.userInfo$.value) {
      this.userData = this.store.userInfo$.value;
      this.auth.userInfo = this.store.userInfo$.value;
      this.auth.updateUserProfile();
      setTimeout(() => {
        this.waitingForUser = false;
      }, 100);
    }
    else {
      this.store.getUser(this.store.userInfo$.value.uid);
      setTimeout(() => {
        this.userData = this.store.userInfo$.value;
        this.waitingForUser = false;
        this.auth.updateUserProfile();
      }, 100);
    }
    this.editProfile = false;
  }

  // checkForUser() {
  //   this.index++
  //   // console.log("🚀 ~ file: account.component.ts ~ line 94 ~ AccountComponent ~ checkForUser ~ this.store.isLoggedEvent", this.store.isLoggedEvent);
  //   if (!this.store.isLoadingUser) {
  //     if ((this.store.userInfo$?.value.uid != undefined || null) && this.auth.isAuthenticated) {
  //       console.log("🚀 ~ file: account.component.ts ~ line 94 ~ AccountComponent ~ checkForUser ~ this.store.isLoadingUser$.value", this.store.isLoadingUser$.value);
  //       console.log("🚀 ~ file: account.component.ts ~ line 69 ~ AccountComponent ~ updateProfile ~ this.store.userInfo$.value", this.store.userInfo$.value);
  //       if (this.store.userInfo$?.value) {
  //         this.auth.userInfo = this.store.userInfo$.value;
  //         setTimeout(() => {
  //           this.waitingForUser = false;
  //         }, 100);
  //       }
  //       else {
  //         this.store.getUser(this.store.userInfo$.value.uid);
  //         setTimeout(() => {
  //           this.waitingForUser = false;
  //           this.auth.updateUserProfile();
  //         }, 100);
  //       }
  //       this.interval.unsubscribe();
  //     } else {
  //       console.log("Not logged in");
  //       if (this.index === 50) {
  //         setTimeout(() => {
  //           this.waitingForUser = false;
  //         }, 100);
  //         this.interval.unsubscribe();
  //       }
  //     }
  //   }
  // }

  async scrollTo(destination: string) {
    if (destination === 'topOfPage') this.scrollToTopOfPage.nativeElement.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });
  }
  async routeToApp(data: ApplicationData) {
    // Reset the accAppData to empty.
    this.store.accAppData$.next(undefined);
    // Set the accAppData to what the user expects.
    this.store.accAppData$.next(data);
    this.router.navigate(['../', 'apply']);
  }

  async showLogin() {
    this.snackBar.open("Please log in.", "Close", { duration: 3000, panelClass: ["info"] });
    let sheet = this._bottomSheet.open(LoginBottomSheet, { panelClass: 'mat-bottom-sheet-container_full-overlay' });
    sheet.afterDismissed().subscribe(x => {
      if (this.auth.isAuthenticated) this.store.getUserData();
      else this._location.back();
      if (!x) {
        this._location.back();
      }
    });
  }

}
