import { AfterViewInit, Component, ElementRef, EventEmitter, Inject, NgZone, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { Location } from '@angular/common';
import { FirebaseApp } from '@angular/fire';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FileUploader } from 'ng2-file-upload';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { from, interval } from 'rxjs';
import { AppState, fxArray } from 'src/app/models';
import { AuthService, LoginBottomSheet } from 'src/app/shared';
import { FirebaseErrors, FireStorageErrors } from 'src/app/shared/models/error.model';
import { ItemsNeeded } from 'src/app/shared/models/user.model';
import * as moment from 'moment';
import { AngularFireStorage } from '@angular/fire/storage';
import { take } from 'rxjs/operators';
import { UserService } from 'src/app/shared/services/user.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { AngularFireAuth } from '@angular/fire/auth';
declare var ImageCompressor: any;

@Component({
  selector: 'app-items-needed',
  templateUrl: './items-needed.component.html',
  styleUrls: ['./items-needed.component.scss'],
  animations: fxArray
})
export class ItemsNeededComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("topOfPage", { static: false }) scrollToTopOfPage: ElementRef;
  @ViewChild("uploadingItems", { static: false }) scrollToUploadingItems: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.store.itemsNeededSource.paginator = this.paginator;
    this.store.itemsNeededSource.sort = this.sort;
  }

  itemsColumns: string[] = ['icon', 'item', 'status', 'priorTo'];
  waitingForUser: boolean = true;
  animate: boolean = false;
  index: number = 0;
  intervalCheck: any;
  eventInterval: any;
  interval: any;
  siteKey = "6LcP950aAAAAAHlsC-29CpZ7Ju8kQLTHHX1acxcx"
  storage: any;
  uploader: FileUploader = new FileUploader({ url: '' });
  uid: string;
  ref: any;
  uploadIndex: number = 0;
  response: string;
  savingNeededItems: boolean = false;
  selectedItem: ItemsNeeded;
  queuedItems: ItemsNeeded[] = [];
  public hasBaseDropZoneOver: boolean = false;
  public hasAnotherDropZoneOver: boolean = false;

  constructor(
    @Inject(FirebaseApp) firebaseApp: any,
    public afAuth: AngularFireAuth,
    public appState: AppState,
    private afStorage: AngularFireStorage,
    public snackBar: MatSnackBar,
    private _bottomSheet: MatBottomSheet,
    public auth: AuthService,
    private _location: Location,
    public store: UserService,
    public ngZone: NgZone, // NgZone service to remove outside scope warning
  ) {
    this.storage = firebaseApp.storage();
    this.uid = localStorage.getItem('uid');
    this.uploader = new FileUploader({
      url: '',
      disableMultipart: true, // 'DisableMultipart' must be 'true' for formatDataFunction to be called.
      formatDataFunctionIsAsync: true,
      formatDataFunction: async (item) => {
        return new Promise((resolve, reject) => {
          resolve({
            name: item._file.name,
            length: item._file.size,
            contentType: item._file.type,
            date: new Date()
          });
        });
      }
    });
    this.hasBaseDropZoneOver = false;
    this.hasAnotherDropZoneOver = false;

    this.response = '';

    this.uploader.response.subscribe(res => this.response = res);
  }

  ngOnInit(): void {
    this.store.accItemsNeeded$.next([]);
    this.selectedItem = new ItemsNeeded();
    this.afAuth.authState.subscribe((user: any) => {
      if (user) {
        this.store.getUser(user.uid);
        this.store.getItemsNeeded()
          .then((data) => {
            this.ngZone.run(() => {
              this.store.getFileUploads();
              setTimeout(() => {
                this.waitingForUser = false;
                this.setDataSourceAttributes();
              }, 100);
            });
          });
      } else {
        localStorage.setItem('user', null);
        this.store.clearUserData();
        this.waitingForUser = false;
        this.showLogin();
      }
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.scrollTo('topOfPage');
    }, 100);
  }

  ngOnDestroy(): void { }

  async scrollTo(destination: string) {
    if (destination === 'topOfPage') this.scrollToTopOfPage.nativeElement.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });
    if (destination === 'uploadingItems') this.scrollToUploadingItems.nativeElement.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  public fileOverAnother(e: any): void {
    this.hasAnotherDropZoneOver = e;
  }

  public onFileSelected(event: EventEmitter<File[]>) {
    let index = 0;
    from(event).pipe(untilDestroyed(this)).subscribe((i: any) => {
      index++
      if (event.length === index) this.prepFile(i, true);
      else this.prepFile(i, false);
    });

    // this.readBase64(file)
    //   .then((data) => {
    //   })

  }

  prepFile(file: File, isLast: boolean) {
    if (this.selectedItem && this.selectedItem.isQueued) {
      this.selectedItem.status = 'Received';
      // const item = { name: file.name, base64: data, lastModified: file.lastModified, uid: this.store.userInfo$.value.uid, user: this.store.userInfo$.value.firstName + ' ' + this.store.userInfo$.value.lastName, fileForItem: this.selectedItem.title, status: 'pending' };

      const item = { name: file.name, originalName: file.name, lastModified: file.lastModified, uid: this.store.userInfo$.value.uid, user: this.store.userInfo$.value.firstName + ' ' + this.store.userInfo$.value.lastName, fileForItem: this.selectedItem.title, status: 'pending', date: moment(moment()).format('MM-DD-YYYY LT'), expires: Date.now() + 1000 * 60 * 60 * 24 * 182.5 };
      item.name = moment(moment()).format('MM-DD-YYYY-HH-MM') + '__' + file.name;
      // item.name = moment(moment()).unix() + '_' + file.name;
      item.name = item.name.split(' ').join('-');
      if (!this.selectedItem.attachedFiles) this.selectedItem.attachedFiles = [];
      this.selectedItem.attachedFiles.push(item);
      this.queuedItems.push(this.selectedItem);
      if (isLast) this.selectedItem = new ItemsNeeded();
    }
    else if (this.selectedItem && !this.selectedItem.isQueued) this.queuedItems.splice(this.queuedItems.indexOf(this.selectedItem), 1)
    else this.snackBar.open("Try selecting a file again.", "Close", { duration: 3000, panelClass: ["error"] });
  }

  readBase64(file): Promise<any> {
    var reader = new FileReader();
    var future = new Promise((resolve, reject) => {
      reader.addEventListener("load", function () {
        resolve(reader.result);
      }, false);

      reader.addEventListener("error", function (event) {
        reject(event);
      }, false);

      reader.readAsDataURL(file);
    });
    return future;
  }

  handleSuccess(event) {
  }

  async uploadDocs() {
    // Needs to be a component bound index due to files being large and finishing upload slower.
    this.uploadIndex = 0;
    this.scrollTo('uploadingItems');
    this.uploader.queue.map((doc, index) => {
      // Change the file name to match what is uploaded to storage and the db.
      doc.file.name = moment(moment()).format('MM-DD-YYYY-HH-MM') + '__' + doc.file.name;
      doc.file.name = doc.file.name.split(' ').join('-');
      // Compressing images
      // If the file is not an image, continue with the the upload.
      const compressor = new ImageCompressor();
      if (doc.file.type != "application/pdf") {
        compressor.compress(doc.file.rawFile, { quality: .5 })
          .then((res) => {
            console.log("Compressed file.");
            doc.file.rawFile = res;
            this.sendUpload(doc);
          }).catch((error) => {
            // Uh-oh, an error occurred!
            console.log("Failed to compress file, continuing upload.", error);
            this.sendUpload(doc);
          });
      } else {
        console.log("PDF, not compressing file.");
        this.sendUpload(doc);
      }
    });
  }

  async sendUpload(doc) {
    var task: any;
    if (doc.progress < 100) var task = this.store.uploadDoc(doc);

    task.percentageChanges().pipe(untilDestroyed(this)).subscribe((x) => {
      doc.progress = x;
    }, (error) => {
      this.snackBar.open(FireStorageErrors.parse(error.code), "Close", { duration: 3000, panelClass: ["error"] });
    }
    );
    if (doc.progress < 100) this.store.uploadDocToMaster(doc)
      .pipe(untilDestroyed(this)).subscribe(x => {
      }, (error) => {
        this.snackBar.open(FireStorageErrors.parse(error.code), "Close", { duration: 3000, panelClass: ["error"] });
      });

    await task.then((res) => {
      if (res.bytesTransferred === res.totalBytes) {
        this.uploadIndex++;
        if (this.uploader?.queue?.length === this.uploadIndex) this.updateItems();
      }
    }).catch((error) => {
      // Uh-oh, an error occurred!
      console.log("Failed to upload.", error);
    });
  }

  async updateItems() {
    let index = 0;
    const customMetadata = {
      uid: this.store.userInfo$.value?.uid,
      cacheControl: 'public,max-age=300',
      contentDisposition: 'attachment',
    };
    this.savingNeededItems = true;
    this.queuedItems.map(item => {
      index++;
      item.attachedFiles.map(file => {
        if (file.status === 'pending') {
          file.status = 'saved';
          var getURL = this.afStorage.refFromURL(`gs://mortgage-partner.appspot.com/mortgage-partner/users/${this.store.userInfo$.value.uid}/${file.name}`);
          // Set the storage item's meta data.
          // Switching the contentDisposition from inline to attachment (this lets us download the file instead of opening it in a new tab).
          getURL.updateMetadata(customMetadata);
          // Get the download url for the file.
          getURL.getDownloadURL().pipe(take(1)).subscribe(x => {
            file.downloadURL = x;
            this.store.updateItemsNeeded(item)
              .then((res) => {
                if (this.queuedItems.length === index) {
                  this.queuedItems = [];
                  this.uploader.clearQueue();
                  // Get the items needed once our files are uploaded (logic could be better).
                  this.store.getItemsNeeded();
                  // Get all the file uploads to update the ui with the current uploads.
                  this.store.getFileUploads();
                  this.snackBar.open('Updated items', "Close", { duration: 3000 });
                }
                this.savingNeededItems = false;
              }).catch((error) => {
                // Uh-oh, an error occurred!
                this.savingNeededItems = false;
                this.snackBar.open(FirebaseErrors.parse(error.code), "Close", { duration: 3000, panelClass: ["error"] });
              });
            this.store.uploadDocToDB(file);
            this.store.uploadDocToMasterDB(file);
          })
          return file;
        }
        else return;
        //
        // ======== Base 64 Version ========
        //
        // this.store.uploadDocToDB(file)
        //   .then((res) => {
        //     this.ngZone.run(() => {
        //       console.log("File uploaded to user");
        //       this.store.uploadDocToMasterDB(file)
        //         .then((res) => {
        //           this.ngZone.run(() => {
        //             console.log("File uploaded to master");
        //           });
        //         }).catch((error) => {
        //           console.log("Server error, please let us know if you see this. Error: ", error);
        //         });
        //     });
        //   }).catch((error) => {
        //     console.log("Upload save error: ", error);
        //     this.snackBar.open(FirebaseErrors.parse(error.code), "Close", { duration: 3000, panelClass: ["error"] });
        //   });
      });
    });
  }

  async queueItemUpdate(item: ItemsNeeded) {
    item.isQueued = true;
    this.selectedItem = item;
    return item;
  }

  async showLogin() {
    this.snackBar.open("Please log in.", "Close", { duration: 3000, panelClass: ["info"] });
    let sheet = this._bottomSheet.open(LoginBottomSheet, { panelClass: 'mat-bottom-sheet-container_full-overlay' });
    sheet.afterDismissed().subscribe(x => {
      if (!x) {
        this._location.back();
      }
    });
  }

  async removedAttached(item) {
    // Removing file from upload queue items to itemsNeeded queue.
    this.queuedItems.map(x => {
      x.attachedFiles.map((d, index) => {
        if (d.name === item.name) x.attachedFiles.splice(index, 1);
        return d;
      })
      // Removing file from upload to Firestore Storage queue.
      this.uploader.queue.map((q, index) => {
        if (q.file.name === item.originalName) this.uploader.queue.splice(index, 1);
        // if (q.name === item.name) q.splice(1, 1);
        // return q;
      })
    })
  }

  async removedFileAttached(item) {
    // Removing file from upload queue items to itemsNeeded queue.
    this.queuedItems.map(x => {
      x.attachedFiles.map((d, index) => {
        if (d.originalName === item.file.name) x.attachedFiles.splice(index, 1);
        return d;
      })
      // Removing file from upload to Firestore Storage queue.
      // CHECK - I think this should be outside the map this is nested in.
      this.uploader.queue.map((q, index) => {
        if (q.file.name === item.file.name) this.uploader.queue.splice(index, 1);
        // if (q.name === item.name) q.splice(1, 1);
        // return q;
      })
    })
  }
  // generateZIP(links) {
  //   console.log('TEST');
  //   var zip = new JSZip();
  //   var count = 0;
  //   var zipFilename = "Pictures.zip";

  //   links.forEach(function (url, i) {
  //     var filename = links[i];
  //     filename = filename.replace(/[\/\*\|\:\<\>\?\"\\]/gi, '').replace("httpsi.imgur.com", "");
  //     // loading a file and add it in a zip file
  //     JSZipUtils.getBinaryContent(url, function (err, data) {
  //       if (err) {
  //         throw err; // or handle the error
  //       }
  //       zip.file(filename, data, { binary: true });
  //       count++;
  //       if (count == links.length) {
  //         zip.generateAsync({ type: 'blob' }).then(function (content) {
  //           saveAs(content, zipFilename);
  //         });
  //       }
  //     });
  //   });
  // }
  downloadFile(file) {

    const downloadLink = document.createElement("a");
    if (file.downloadURL !== undefined) {
      downloadLink.setAttribute("href", file.downloadURL);
      downloadLink.setAttribute("target", "_blank");
      downloadLink.style.visibility = 'hidden';
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
    }
    // console.log("🚀 ~ file: items-needed.component.ts ~ line 320 ~ ItemsNeededComponent ~ downloadFile ~ file", file);
    // // const linkSource = file.base64;
    // const linkSource = file.downloadURL;
    // const fileName = file.name;

    // downloadLink.href = linkSource;
    // downloadLink.target = '_blank';
    // downloadLink.download = fileName;
    // document.body.appendChild(downloadLink);

    // downloadLink.click();
  }


  downloadAllFiles() {
    this.store.accItemsNeeded$.value.map(x => {
      if (x.attachedFiles) x.attachedFiles.map(file => {
        setTimeout(() => {
          this.downloadFile(file);
        }, 750);
      });
    });
  }

  // downloadFile(b64encodedString: string) {
  //   if (b64encodedString) {
  //     var blob = this.base64ToBlob(b64encodedString, 'text/plain');
  //     saveAs(blob, "test.properties");
  //   }
  // }


  // public base64ToBlob(b64Data, contentType = '', sliceSize = 512) {
  //   b64Data = b64Data.replace(/\s/g, ''); //IE compatibility...
  //   let byteCharacters = atob(b64Data);
  //   let byteArrays = [];
  //   for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
  //     let slice = byteCharacters.slice(offset, offset + sliceSize);

  //     let byteNumbers = new Array(slice.length);
  //     for (var i = 0; i < slice.length; i++) {
  //       byteNumbers[i] = slice.charCodeAt(i);
  //     }
  //     let byteArray = new Uint8Array(byteNumbers);
  //     byteArrays.push(byteArray);
  //   }
  //   return new Blob(byteArrays, { type: contentType });
  // }

}

function FirebaseRef(FirebaseRef: any) {
  throw new Error('Function not implemented.');
}
