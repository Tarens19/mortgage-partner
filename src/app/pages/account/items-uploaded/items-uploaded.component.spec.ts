import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsUploadedComponent } from './items-uploaded.component';

describe('ItemsUploadedComponent', () => {
  let component: ItemsUploadedComponent;
  let fixture: ComponentFixture<ItemsUploadedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemsUploadedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsUploadedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
