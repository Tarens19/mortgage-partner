import { AfterViewInit, Component, ElementRef, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { FirebaseApp } from '@angular/fire';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppState, fxArray } from 'src/app/models';
import { AuthService, LoginBottomSheet } from 'src/app/shared';
import { UserService } from 'src/app/shared/services/user.service';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-items-uploaded',
  templateUrl: './items-uploaded.component.html',
  styleUrls: ['./items-uploaded.component.scss'],
  animations: fxArray
})
export class ItemsUploadedComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild("topOfPage", { static: false }) scrollToTopOfPage: ElementRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }


  filter: string = '';
  waitingForUser: boolean;
  uploadsColumns: string[] = ['icon', 'name', 'date', 'for', 'action'];

  constructor(
    @Inject(FirebaseApp) firebaseApp: any,
    public afAuth: AngularFireAuth,
    public appState: AppState,
    public snackBar: MatSnackBar,
    public auth: AuthService,
    private _bottomSheet: MatBottomSheet,
    private _location: Location,
    public store: UserService,
  ) { }

  ngOnInit(): void {
    this.afAuth.authState.subscribe((user: any) => {
      if (user) {
        this.store.getUser(user.uid);
        this.store.getFileUploads();
      } else {
        localStorage.setItem('user', null);
        this.store.clearUserData();
        this.showLogin();
      }
      this.waitingForUser = false;
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.scrollTo('topOfPage');
    }, 100);
    setTimeout(() => {
      this.setDataSourceAttributes();
    }, 1000);
  }

  ngOnDestroy(): void { }


  setDataSourceAttributes() {
    this.store.uploadsSource.paginator = this.paginator;
    this.store.uploadsSource.sort = this.sort;
  }

  async scrollTo(destination: string) {
    if (destination === 'topOfPage') this.scrollToTopOfPage.nativeElement.scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });
  }

  async showLogin() {
    this.snackBar.open("Please log in.", "Close", { duration: 3000, panelClass: ["info"] });
    let sheet = this._bottomSheet.open(LoginBottomSheet, { panelClass: 'mat-bottom-sheet-container_full-overlay' });
    sheet.afterDismissed().subscribe(x => {
      if (!x) {
        this._location.back();
      }
    });
  }

  downloadFile(file) {

    const downloadLink = document.createElement("a");
    if (file.downloadURL !== undefined) {
      downloadLink.setAttribute("href", file.downloadURL);
      downloadLink.setAttribute("target", "_blank");
      downloadLink.style.visibility = 'hidden';
      document.body.appendChild(downloadLink);
      downloadLink.click();
      document.body.removeChild(downloadLink);
    }
    // console.log("🚀 ~ file: items-needed.component.ts ~ line 320 ~ ItemsNeededComponent ~ downloadFile ~ file", file);
    // // const linkSource = file.base64;
    // const linkSource = file.downloadURL;
    // const fileName = file.name;

    // downloadLink.href = linkSource;
    // downloadLink.target = '_blank';
    // downloadLink.download = fileName;
    // document.body.appendChild(downloadLink);

    // downloadLink.click();
  }


  downloadAllFiles() {
    this.store.accUploads$.value.map(x => {
      setTimeout(() => {
        this.downloadFile(x);
      }, 750);
    });
  }

  // Filter
  async applyFilter(filterValue: string) {
    this.store.uploadsSource.filter = filterValue.trim().toLowerCase();
  }

  async resetFilter() {
    this.filter = '';
    this.applyFilter('');
  }
}
