import { AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { fxArray } from 'src/app/models';

@Component({
  selector: 'app-loan-limits',
  templateUrl: './loan-limits.component.html',
  styleUrls: ['./loan-limits.component.scss'],
  animations: fxArray
})
export class LoanLimitsComponent implements OnInit, AfterViewInit {

  dataSource: MatTableDataSource<any>;
  filter: string = '';
  limitsColumns: string[] = ['county', 'conventional', 'fha'];
  limits = [
    {county: "Beaver County", convAmount: 647200, fhaAmount: 420680},
    {county: "Box Elder County", convAmount: 647200, fhaAmount: 646300},
    {county: "Cache County", convAmount: 647200, fhaAmount: 420680},
    {county: "Carbon County", convAmount: 647200, fhaAmount: 420680},
    {county: "Daggett County", convAmount: 647200, fhaAmount: 420680},
    {county: "Davis County", convAmount: 647200, fhaAmount: 646300},
    {county: "Duchesne County", convAmount: 647200, fhaAmount: 420680},
    {county: "Emery County", convAmount: 647200, fhaAmount: 420680},
    {county: "Garfield County", convAmount: 647200, fhaAmount: 420680},
    {county: "Grand County", convAmount: 647200, fhaAmount: 420680},
    {county: "Iron County", convAmount: 647200, fhaAmount: 420680},
    {county: "Juab County", convAmount: 647200, fhaAmount: 508300},
    {county: "Kane County", convAmount: 647200, fhaAmount: 420680},
    {county: "Millard County", convAmount: 647200, fhaAmount: 420680},
    {county: "Morgan County", convAmount: 647200, fhaAmount: 646300},
    {county: "Piute County", convAmount: 647200, fhaAmount: 420680},
    {county: "Rich County", convAmount: 647200, fhaAmount: 447350},
    {county: "Salt Lake County", convAmount: 647200, fhaAmount: 523250},
    {county: "San Juan County", convAmount: 647200, fhaAmount: 420680},
    {county: "Sanpete County", convAmount: 647200, fhaAmount: 420680},
    {county: "Sevier County", convAmount: 647200, fhaAmount: 420680},
    {county: "Summit County", convAmount: 970800, fhaAmount: 970800},
    {county: "Tooele County", convAmount: 647200, fhaAmount: 523250},
    {county: "Uintah County", convAmount: 647200, fhaAmount: 420680},
    {county: "Utah County", convAmount: 647200, fhaAmount: 508300},
    {county: "Wasatch County", convAmount: 970800, fhaAmount: 970800},
    {county: "Washington County", convAmount: 647200, fhaAmount: 481850},
    {county: "Wayne County", convAmount: 647200, fhaAmount: 420680},
    {county: "Weber County", convAmount: 647200, fhaAmount: 646300},
  ]
  private sort: MatSort;

  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  constructor(
    private cdr: ChangeDetectorRef
    ) { }

  ngOnInit(): void {
    this.dataSource = new MatTableDataSource();
  }

  ngAfterViewInit() {
    // If the user changes the sort order, reset back to the first page.
    // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    this.dataSource = new MatTableDataSource(this.limits);
    this.dataSource.sort = this.sort;
    this.cdr.detectChanges();
  }

  // All other filters
  async applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setDataSourceAttributes() {
    this.dataSource.sort = this.sort;
  }

  async resetFilter() {
    this.filter = '';
    this.applyFilter('');
  }

}
