import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoanLimitsComponent } from './loan-limits.component';

describe('LoanLimitsComponent', () => {
  let component: LoanLimitsComponent;
  let fixture: ComponentFixture<LoanLimitsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoanLimitsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoanLimitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
