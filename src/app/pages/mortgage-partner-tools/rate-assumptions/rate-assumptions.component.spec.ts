import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RateAssumptionsComponent } from './rate-assumptions.component';

describe('RateAssumptionsComponent', () => {
  let component: RateAssumptionsComponent;
  let fixture: ComponentFixture<RateAssumptionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RateAssumptionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RateAssumptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
