import { NgModule } from '@angular/core';
import { TextMaskModule } from 'angular2-text-mask';
import { AppCommonModule } from 'src/app/app-common.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { FaqsComponent, LoanCalculatorsComponent, LoanLimitsComponent, MortgageGlossaryComponent, PrequalificationCalculatorComponent, RateAssumptionsComponent, ToolsComponent } from '.';
import { ToolsRoutingModule } from './tools.routing';

@NgModule({
	imports: [
		AppCommonModule,
		SharedModule,
		TextMaskModule,
		ToolsRoutingModule,
	],
	declarations: [
		ToolsComponent,
		FaqsComponent, 
		LoanCalculatorsComponent, 
		LoanLimitsComponent, 
		MortgageGlossaryComponent, 
		PrequalificationCalculatorComponent, 
		RateAssumptionsComponent
	],
	providers: [
		// Services here
	],
	exports: [],
	entryComponents: [
		// Dialogs, bottomsheets. etc. here.
	]
})
export class ToolsModule {
	constructor() { console.log('@ModuleCreated::ToolsModule'); }
}
