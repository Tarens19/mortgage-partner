import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrequalificationCalculatorComponent } from './prequalification-calculator.component';

describe('PrequalificationCalculatorComponent', () => {
  let component: PrequalificationCalculatorComponent;
  let fixture: ComponentFixture<PrequalificationCalculatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrequalificationCalculatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrequalificationCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
