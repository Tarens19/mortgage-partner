import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FaqsComponent, LoanCalculatorsComponent, LoanLimitsComponent, MortgageGlossaryComponent, PrequalificationCalculatorComponent, RateAssumptionsComponent, ToolsComponent } from '.';


const routes: Routes = [
	{
		path: '',
		children: [
			{ path: '', component: ToolsComponent, data: { title: 'Tools' } },
			{ path: 'utah-mortgage-glossary', component: MortgageGlossaryComponent, data: { title: 'Everything You Need To Know About Mortgages' } },
			{ path: 'utah-mortgage-faqs', component: FaqsComponent, data: { title: 'Utah Mortgage Frequently Asked Questions' } },
			{ path: 'utah-mortgage-loan-limits', component: LoanLimitsComponent, data: { title: 'Utah Mortgage Loan Limit' } },
			{ path: 'utah-mortgage-loan-pre-qualification', component: PrequalificationCalculatorComponent, data: { title: 'Get Pre-Qualified For A Mortgage Loan In Utah' } },
			{ path: 'utah-mortgage-loan-calculators', component: LoanCalculatorsComponent, data: { title: 'Calculate Your Mortgage Loan In Utah' } },
			{ path: 'utah-mortgage-rate-assumptions', component: RateAssumptionsComponent, data: { title: 'Estimating Your Mortgage Loan In Utah' } },
		]
	},
	{ path: '**', redirectTo: '/404', pathMatch: 'full' }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ToolsRoutingModule { }
