// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyA8lWz4RATAEoFSXpC58MPO9De94wFnYT0",
    authDomain: "mortgage-partner.firebaseapp.com",
    databaseURL: "https://mortgage-partner-default-rtdb.firebaseio.com",
    projectId: "mortgage-partner",
    storageBucket: "mortgage-partner.appspot.com",
    messagingSenderId: "1061785793247",
    appId: "1:1061785793247:web:c65b809289f9716a91ef6a",
    measurementId: "G-0G7207FXMZ"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
